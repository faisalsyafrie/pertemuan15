from django.urls import path
from .views import index, AlatDetailView, AlatCreateView #new
from .views import AlatEditView, AlatDeleteView, AlatToPdf #new

urlpatterns = [
	path('', index, name='home_page'),
	path('pemesan/<int:pk>', AlatDetailView.as_view(), #new
		name='alat_detail_view'),
	path('pemesan/add', AlatCreateView.as_view(), name='alat_add'), #new
	path('pemesan/edit/<int:pk>', AlatEditView.as_view(), name='alat_edit'),
	path('pemesan/delete/<int:pk>', AlatDeleteView.as_view(), name='alat_delete'),
	path('pemesan/print_pdf', AlatToPdf.as_view(), name='alat_to_pdf') #new
]