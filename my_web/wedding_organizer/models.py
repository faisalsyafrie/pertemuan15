from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse #new
from django.utils import timezone
# Create your models here.

class Pemesan(models.Model):
	PAKET_CHOICES = (
		('vn','Vian (Rp. 17.000.000)'),
		('an','Andrea (Rp. 37.000.000)'),
		('ov','Oval (Rp. 20.000.000)'),
	)
	JK_CHOICES = (
		('l','Laki-laki'),
		('p','Perempuan'),
	)
	nama_pemesan = models.CharField('Nama Pemesan', max_length=50, null=False)
	email = models.CharField('Email', max_length=50, null=False)
	paket = models.CharField(max_length=2, choices=PAKET_CHOICES)
	jk = models.CharField(max_length=1, choices=JK_CHOICES)
	alamat = models.TextField()
	no_telp = models.IntegerField()
	tgl_input = models.DateTimeField('Tgl. Pemesanan', default=timezone.now)
	user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

	class Meta:
		ordering = ['-tgl_input']

	def __str__(self):
		return self.nama_pemesan

	def get_absolute_url(self):
		return reverse('home_page')
