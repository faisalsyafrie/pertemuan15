

from .models import Pemesan #new
from django.shortcuts import render
from django.views.generic import DetailView, CreateView #new
from django.views.generic import UpdateView #new
from django.urls import reverse_lazy #new
from django.views.generic import DetailView
from django.views.generic import DeleteView
from .utils import Render
from django.views.generic import View
# Create your views here.
var = {
	'judul' : 'Sistem Informasi Wedding Organizer',
	'info' : '''Kami menyediakan jasa wedding organizer secara online''',
	'oleh' : 'owner'
}

def index(self):
	#new
	var['pemesan'] = Pemesan.objects.values('id', 'nama_pemesan', 'paket').\
	order_by('nama_pemesan')
	return render(self, 'wedding_organizer/index.html',context=var)

class AlatDetailView(DetailView): #new
	model = Pemesan
	template_name = 'wedding_organizer/alat_detail_view.html'

	def get_content_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AlatCreateView(CreateView): #new
	model = Pemesan
	fields = '__all__'
	template_name = 'wedding_organizer/alat_add.html'

	def get_content_data(self, **kwargs):
		context = var
		context.update(super().get_content_data(**kwargs))
		return context

class AlatEditView(UpdateView):
	model = Pemesan
	fields = ['nama_pemesan','paket','jk','email','alamat','no_telp']
	template_name= 'wedding_organizer/alat_edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AlatDeleteView(DeleteView):
	model = Pemesan
	template_name = 'wedding_organizer/alat_delete.html'
	success_url= reverse_lazy('home_page')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

#new 

class AlatToPdf(View):
	def get(self,request):
		var = {
			'pemesan' : Pemesan.objects.values(
				'nama_pemesan','paket','no_telp','alamat'),
			'request':request
		}
		return Render.to_pdf(self,'wedding_organizer/alat_to_pdf.html',var)

